API REST − Partage de frais d'essence
=====================================

Cette API représente constitue le *back-end* d'une application pour gérer les
frais d'essence sur un véhicule partagé.

Plusieurs personnes partagent une voiture ; chacune peut :

- Utiliser la voiture pour rouler (et donc faire à chaque trajet un certain nombre de kms)
- Mettre de l'essence dans la voiture (une certaine somme en €)

Types de données
----------------

Cette API gère plusieurs types de ressources :

- un véhicule  : `vehicle`
- des conducteurs : `drivers`
- des voyages : `journeys`
- des pleins d'essence : `refils`

NB : à des fins de simplicité, cette API

- ne gère aucune pagination
- ne gère aucune authentification.
- ne permet de gérer qu'un véhicule

URLs
----

**NB: quand on note `/truc/:id` cela signifie qu'il faudra dans la réalité
remplacer « `:id` » par l'`id` de la ressource (qui est un nombre). Ex pour
récupérer le *truc* dont l'`id` est 1 : `/truc/1`.**


Les ressources `driver` `journey` et `refil` partagent la même structure d'URL.
Voici un exemple pour les `driver` :

|                                        | méthode HTTP   |              URL |
|----------------------------------------|:--------------:|-----------------:|
| Listing                                |          `GET` | `/api/drivers`   |
| Création d'un élément                  |         `POST` | `/api/drivers`   |
| Récupération d'un élément              |          `GET` | `/api/drivers/:id` |
| Suppression d'un élément               |       `DELETE` | `/api/drivers/:id` |
| Modification (écrasement) d'un élément |          `PUT` | `/api/drivers/:id` |


La structure d'URL pour le `vehicle` est plus simple (car il n'y a qu'un
véhicule) :

|                                        | méthode HTTP   |              URL |
|----------------------------------------|:--------------:|-----------------:|
| Récupération d'un élément              |          `GET` | `/api/vehicle`   |
| Modification (écrasement) d'un élément |          `PUT` | `/api/vehicle`   |

Format des ressources
---------------------

Toutes les ressources sont échangées au format **JSON**. Un exemple de contenu
est doné pour chaque ressource :

### Vehicle

```json
{
    "model": "ZX Break 1.9D Audace",
    "year": 1996
}
```


### Driver

```json
{
    "id": 1,
    "name": "Alice"
}

```

### Refil

```json
{
    "id": 1,
    "driverId": 1,
    "cost": 54
}
```
*NB: le `driverId` doit correspondre à un `driver` existant*.

### Journey

```json
{
    "id": 1,
    "driverId": 1,
    "distance": 10
}
```
*NB: le `driverId` doit correspondre à un `driver` existant*.

Endpoints
---------

### Vehicle

#### GET /api/vehicle

Corps de la requête : néant.

Corps de la réponse : une ressource `vehicle`.

Ex:
```json
{
    "model": "ZX Break 1.9D Audace",
    "year": 1996
}
```

Code de retour :

- 200 si on a un véhicule à renvoyer
- 404 si on a pas de véhicule à renvoyer (non configuré)


#### PUT /api/vehicle

Corps de la requête : une ressource `vehicle`.
```json
{
    "model": "ZX Break 1.9D Audace",
    "year": 1996
}
```

Corps de la réponse : néant

Code de retour :

- 201 si le contenu a été accepté et sauvegardé
- 400 si le format du corps de la requête n'est pas bon

### Driver

#### GET /api/drivers

Corps de la requête : néant.

Corps de la réponse : une *tableau* de ressources `driver`.

Ex :
```json
[
    {
        "id": 1,
        "name": "Alice"
    },
    {
        "id": 2,
        "name": "Bob"
    }
]
```

Code de retour :

- 200 (même si la liste est vide)

#### GET /api/drivers/:id

Corps de la requête : néant.

Corps de la réponse : une ressource `driver`.

Ex pour `GET /api/drivers/1` :
```json
{
    "id": 1,
    "name": "Alice"
}
```

Code de retour :

- 200 si une ressource est retournée
- 404 si la ressource correspondant à l'`id` demandé n'existe pas

#### POST /api/drivers

Corps de la requête : une ressource `driver` que l'on souhaite créer.

Ex :
```json
{
    "name": "Alice"
}
```
*NB: on ne mentionne pas l'ID puisqu'il est attribué automatiquement par le
serveur.*

Corps de la réponse : néant

Code de retour:

- 201 si la modification a été acceptée et enregistrée par le serveur
- 400 si le contenu du corps de la requête est invalide


#### PUT /api/drivers/:n

Corps de la requête : une ressource `driver`.

Ex pour `PUT /api/drivers/1` :
```json
{
    "id": 1,
    "name": "Alice2"
}
```

*NB: par commodité, l'`id` peut être mentionné optionellement, mais il sera
ignoré par le serveur (il n'est pas possible de modifier le champ `id`).*

Corps de la réponse : néant

Code de retour :

- 204 si la modification a été acceptée et enregistrée par le serveur
- 404 si la ressource correspondant à l'`id` demandé n'existe pas
- 400 si le contenu du corps de la requête est invalide

#### DELETE /api/drivers/:n

Corps de la requête : néant

Corps de la réponse : néant

Code de retour :

- 204 si la modification a été acceptée et enregistrée par le serveur
- 404 si la ressource correspondant à l'`id` demandé n'existe pas

### Journey

Même logique que pour `driver`, mais en manipulant des ressources de type `journey`.

### Refill
Même logique que pour `driver`, mais en manipulant des ressources de type `refill`.
