package com.codelutin.examples;

import com.codelutin.DriverDao;
import com.codelutin.beans.Driver;
import com.codelutin.beans.Journey;
import com.codelutin.beans.Vehicle;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class VehicleShareTest extends TestCase {

    public void testBasic() throws Exception {

        Driver yannick = new Driver("Yannick");
        Vehicle ceed = new Vehicle("Kia Ceed", 2016, 8.5f, 45);

//        ceed.refill(45, yannick);

        System.out.println(ceed.getModel() + " contient encore " + ceed.getFuel() + "L.");

        Journey firstJourney = new Journey();
        firstJourney.setDistance(20);
        firstJourney.addDriver(yannick);
        System.out.println(firstJourney.display());

        ceed.addJourney(firstJourney);
        System.out.println(ceed.getModel() + " contient encore " + ceed.getFuel() + "L.");

        Driver paul = new Driver("Paul");

        Journey secondJourney = new Journey();
        secondJourney.setDistance(100);
        secondJourney.addDriver(yannick);
        secondJourney.addDriver(paul);
        System.out.println(secondJourney.display());

        ceed.addJourney(secondJourney);
        System.out.println(ceed.getModel() + " contient encore " + ceed.getFuel() + "L.");

        Journey thirdJourney = new Journey();
        thirdJourney.setDistance(150);
        thirdJourney.addDriver(yannick);
        ceed.addJourney(thirdJourney);
        System.out.println(thirdJourney.display());

        System.out.println(ceed.getModel() + " contient encore " + ceed.getFuel() + "L.");
    }

    public void testDriverDao() {

        String driverName = "Yannick";
        Driver driver = new Driver(driverName);
        DriverDao driverDao = new DriverDao();
        Integer driverId = driverDao.save(driver);
        System.out.println("Conducteur persisté avec id=" + driverId);

        Driver loadDriver = driverDao.load(driverId);
        Assert.assertEquals(driverName, loadDriver.getName());
    }
}
