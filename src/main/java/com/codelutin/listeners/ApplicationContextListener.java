package com.codelutin.listeners;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @author ymartel (martel@codelutin.com)
 */
@WebListener
public class ApplicationContextListener implements ServletContextListener {

    private static EntityManagerFactory emf;
    final private static String PERSISTENCE_UNIT = "com.codelutin.formationjava.jpa";

    public void contextInitialized(ServletContextEvent event) {
        System.out.println("+++ ServletContextListener : contextInitialized - Inititalizing EMF for PU: " + PERSISTENCE_UNIT);
        emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
        System.out.println("+++ ServletContextListener : contextInitialized - Init EMF done for PU: " + PERSISTENCE_UNIT);
    }

    public void contextDestroyed(ServletContextEvent event) {
        System.out.println("+++ ServletContextListener : contextDestroyed - Closing EMF for PU: " + PERSISTENCE_UNIT);
        emf.close();
        System.out.println("+++ ServletContextListener : contextDestroyed - Closed EMF done for PU " + PERSISTENCE_UNIT);
    }

    public static EntityManager createEntityManager() {
        if (emf == null) {
            throw new IllegalStateException("Context is not initialized yet.");
        }
        return emf.createEntityManager();
    }
}
