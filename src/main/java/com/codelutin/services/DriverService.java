package com.codelutin.services;

import com.codelutin.DriverDao;
import com.codelutin.beans.Driver;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Path("/drivers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DriverService {

    protected DriverDao driverDao = new DriverDao();

    @GET
    @Path("")
    public List<Driver> printHello() {
        List<Driver> all = driverDao.getAll();
        if (all == null || all.isEmpty()) {
            // Make some fake datas...
            Driver fakeOne = new Driver();
            fakeOne.setId(-1);
            fakeOne.setName("FakeOne");
            all.add(fakeOne);
            Driver fakeTwo = new Driver();
            fakeTwo.setId(-2);
            fakeTwo.setName("FakeTwo");
            all.add(fakeTwo);
        }
        return all;
    }

    @PUT
    @Path("")
    public Response createDriver(Driver driver) {
        if (driver == null) {
            return Response.status(400).build();
        }
        driverDao.save(driver);
        return Response.status(201).build();
    }

}
