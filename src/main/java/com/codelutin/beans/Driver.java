package com.codelutin.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Entity
public class Driver implements Serializable {

    private static final long serialVersionUID = 5142397940988549172L;

    @Id
    @GeneratedValue
    protected Integer id;

    protected String name;

    public Driver() {}

    public Driver(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Driver driver = (Driver) o;
        return Objects.equals(id, driver.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
