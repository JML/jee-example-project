package com.codelutin.exceptions;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class NotEnoughFuelException extends Exception {

    private static final long serialVersionUID = -2228767991445937414L;

    protected float possibleKm;

    public NotEnoughFuelException(float possibleKm, String message) {
        super(message);
        this.possibleKm = possibleKm;
    }

    public float getPossibleKm() {
        return possibleKm;
    }
}
