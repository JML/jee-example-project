package com.codelutin.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class NotEnoughFuelExceptionMapper implements ExceptionMapper<IllegalArgumentException> {

    public Response toResponse(IllegalArgumentException exception) {
        return Response.status(412).entity(exception).build();
    }
}
