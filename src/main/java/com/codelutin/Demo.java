package com.codelutin;

import java.util.Arrays;

/**
 * Hello world!
 */
public class Demo {

    public static void main(String[] args) {
        int tab1[] = {2, 4, 6, 7};
        int tab2[];
        tab2 = Arrays.copyOf(tab1, 3);
        System.out.println("tab2 length" + tab2.length);
        System.out.println("tab2[0]=" + tab2[0]);
        tab2[0]=100;
        System.out.println("tab2[0]=" + tab2[0]);
        System.out.println("tab1[0]=" + tab1[0]);
    }
}
