package com.codelutin;

import com.codelutin.exceptions.NotEnoughFuelExceptionMapper;
import com.codelutin.services.DriverService;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ymartel (martel@codelutin.com)
 */
@ApplicationPath("/api")
public class VehicleShareApplication extends Application {

    public VehicleShareApplication() {}

    @Override
    public Set<Object> getSingletons() {
        Set<Object> set = new HashSet<Object>();
        set.add(new DriverService());
        return set;
    }

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> set = new HashSet<Class<?>>();
        set.add(NotEnoughFuelExceptionMapper.class);
        return set;
    }
}
